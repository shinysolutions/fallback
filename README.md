# FallBack Plan Plugin #

This is a plugin for the Joomla Component Membership Pro from JoomDonations.  It allows an expired member to drop to a lower (free) plan automatically.  It can be chained together so members can drop to another lower plan etc.

### How do I get set up? ###

* Install the plugin in Joomla Extensions as normal.
* Publish the plugin
* On the Membership Pro Plans select which free (if any) plan the member should 'drop' down to