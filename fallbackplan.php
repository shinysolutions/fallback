<?php
/**
 * @version        2.4
 * @package        Joomla
 * @subpackage     Membership Pro
 * @author         Calum Polwart based on GPL code from OSMembership Plugin by Tuan Pham Ngoc
 * @copyright      Copyright (C) 2016 - 2017 Shiny Solutions & OS Solutions
 * @license        GNU/GPL, see LICENSE.php
 */
defined('_JEXEC') or die;

class plgOSMembershipFallbackplan extends JPlugin
{
    public function __construct(& $subject, $config)
    {
            parent::__construct($subject, $config);

            JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_osmembership/table');
    }

    /**
     * Render setting form
     *
     * @param PlanOSMembership $row
     *
     * @return array
     */
    public function onEditSubscriptionPlan($row)
    {

            ob_start();
            $this->drawSettingForm($row);
            $form = ob_get_contents();
            ob_end_clean();

            return array('title' => 'FallBack Plan',
                         'form'  => $form
            );
            

    }

    /**
     * Store setting into database, in this case, use params field of plans table
     *
     * @param PlanOsMembership $row
     * @param bool             $isNew true if create new plan, false if edit
     */
    public function onAfterSaveSubscriptionPlan($context, $row, $data, $isNew)
    {
            
            $params = new JRegistry($row->params);

            $params->set('fallbackplan_Id',  $data['fallbackplan_Id']);
            $row->params = $params->toString();

            $row->store();
            
    }



    public function onMembershipActive($row)
    {
        $plugin = JPluginHelper::getPlugin('osmembership', 'fallbackplan');
        $params = new JRegistry($plugin->params);
                


        //$rosterId = $params->get('roster_id','');

          
   
    }

    public function onMembershipExpire($current)
    {

        
        // Ensure the user id is really an int
        $userId =  $current->user_id;

        // If the user id appears invalid then bail out just in case
        if (empty($userId))
        {
                return false;
        }

        $planId = $current->plan_id;  //current plan
        
        $fallBackId = NULL ; //fallback plan
        
        
        if (empty($planId))
        {
                return false;
        }

        if (!file_exists(JPATH_ROOT . '/components/com_osmembership/osmembership.php'))
        {
                return;
        }
        
        // Get a db connection.
        $db = JFactory::getDbo();
 
        // Create a new query object.
        $query = $db->getQuery(true);
 
        $query
        ->select($db->quoteName('params'))
        ->from($db->quoteName('#__osmembership_plans'))
        ->where($db->quoteName('id') . ' = '. $db->quote($planId));
 
        // Reset the query using our newly populated query object.
        $db->setQuery($query);
 
        // Load the results as a list of stdClass objects (see later for more options on retrieving data).
        $result = $db->loadResult();
        $planParams = json_decode($result, true);
        
        //check if fall back plan is defined
         if (array_key_exists("fallbackplan_Id", $planParams)) {
             if ($planParams['fallbackplan_Id'] > 0 )
             {
                 $fallBackId = $planParams['fallbackplan_Id'];
             }
         }
         
         if (is_null($fallBackId)) {
             return false ;
         }
             
         //check if the member has another valid plan - if so don't fall back this one.
         
                // Get a db connection.
        $db2 = JFactory::getDbo();
 
        // Create a new query object.
        $query2 = $db2->getQuery(true);
 
        $query2
        ->select($db2->quoteName('id'))
        ->from($db2->quoteName('#__osmembership_subscribers'))
        ->where($db2->quoteName('user_id') . ' = '. $db2->quote($userId) and $db2->quoteName('published') . ' <  2'  );
 
        // Reset the query using our newly populated query object.
        $db2->setQuery($query2);
 
        // Load the results as a list of stdClass objects (see later for more options on retrieving data).
        $result2 = $db2->loadResult();

        if (count(get_object_vars($result2))> 1) {
            //2 or more active or pending subscriptions so don't create a f/back
            return false;
        }
         

/*
 * This section in the copied code disables the plugin if the changes are made to the subscription status from OSM manually.
 * 
 * Consider TODO to switch on and off in plugin params
 * 
        $input  = JFactory::getApplication()->input;
        $option = $input->getCmd('option');
        if ($option == 'com_osmembership')
        {
                return false;
        }
 * 
 */
        $db       = JFactory::getDbo();
        $nullDate = $db->getNullDate();

        // Create subscription record
        require_once JPATH_ADMINISTRATOR . '/components/com_osmembership/loader.php';
        JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_osmembership/table');
        $row     = JTable::getInstance('osmembership', 'Subscriber');
        $rowPlan = JTable::getInstance('osmembership', 'Plan');
        $rowPlan->load($fallBackId);

        // Copy over the subscription from the old table
        foreach ( $current as $key=>$value ){
            $row->$key = $value;
        }
        //Update the plan with the new plan 
        $row->plan_id = $fallBackId;
        /*
        $row->user_id = $current->user_id;
        $row->email   = $current->user_email;
        $row->first_name = $current->first_name;
        $row->last_name = $current->last_name;
         * */
         
        //update the dates
        $row->created_date = JFactory::getDate()->toSql();
        $date              = JFactory::getDate();
        $row->from_date    = $date->toSql();
        $row->from_date    = JFactory::getDate()->toSql();

        // Calculate price, from date, to date for the subscription record
        // This is redudent code as only allowing Fallback to free but remains here in case of updates to fall back to paid in future.
        if ($rowPlan->expired_date && $rowPlan->expired_date != $nullDate)
        {
                $expiredDate = JFactory::getDate($rowPlan->expired_date);

                // Change year of expired date to current year
                $expiredDate->setDate(JFactory::getDate()->format('Y'), $expiredDate->month, $expiredDate->day);
                $expiredDate->setTime(0, 0, 0);
                $startDate = clone $date;
                $startDate->setTime(0, 0, 0);

                if ($startDate >= $expiredDate)
                {
                        $date->setDate($date->year + 1, $expiredDate->month, $expiredDate->day);
                        $row->to_date = $date->toSql();
                }
                else
                {
                        $row->to_date = $rowPlan->expired_date;
                }
        }
        else
        {
                if ($rowPlan->lifetime_membership)
                {
                        $row->to_date = '2099-12-31 23:59:59';
                }
                else
                {
                        $dateIntervalSpec = 'P' . $rowPlan->subscription_length . $rowPlan->subscription_length_unit;
                        $row->to_date     = $date->add(new DateInterval($dateIntervalSpec))->toSql();
                }
        }

        $row->amount                 = $rowPlan->price;
        $row->discount_amount        = 0;
        $row->tax_amount             = 0;
        $row->payment_processing_fee = 0;
        $row->gross_amount           = $rowPlan->price;
        
        // Set a few fields to indicate how this plan got applied:
        $row->subscription_code = NULL; 
        $row->payment_method = "fallback"; 
        $row->transaction_id = "fallback";
        $row->from_subscription_id = $current->subscription_id; 
        
        //Reset some of the subs settings.
        $row->id = NULL;
        $row->first_reminder_sent = 0; 
        $row->second_reminder_sent = 0 ;
        $row->third_reminder_sent = 0;
        $row->payment_made = 0; 
        $row->params = null;
        $row->recurring_profile_id = null;
        $row->subscription_id = NULL;
        $row->recurring_subscription_cancelled = 0 ;
        $row->renewal_count = 0 ;
        $row->from_plan_id = $current->plan_id;
        $row->invoice_number = 0 ;


        $row->first_reminder_sent_at = NULL;
        $row->second_reminder_sent_at = NULL;
        $row->third_reminder_sent_at = NULL;
        
        
        $row->published              = 1;
        $row->store();

        // Store profile ID
        $row->profile_id = $row->id;
        $row->store();

        JPluginHelper::importPlugin('osmembership');
        $dispatcher = JDispatcher::getInstance();
        $dispatcher->trigger('onAfterStoreSubscription', array($row));
        $dispatcher->trigger('onMembershipActive', array($row));

        // Store custom fields data if Joomla core user-profile is enabled
        if (JPluginHelper::isEnabled('user', 'profile'))
        {
                $rowFields   = OSMembershipHelper::getProfileFields($row->plan_id, true);
                //TODO - Profile copy
                $formData    = $input->post->get('jform', array(), 'array');
                $profileData = isset($formData['profile']) ? $formData['profile'] : array();
                $customFieldData = array();
                if (count($formData))
                {				
                        foreach ($rowFields as $rowField)
                        {
                                if ($rowField->profile_field_mapping && !empty($profileData[$rowField->profile_field_mapping]))
                                {
                                        if ($rowField->is_core)
                                        {
                                                $row->{$rowField->name} = $profileData[$rowField->profile_field_mapping];
                                        }
                                        else
                                        {
                                                $customFieldData[$rowField->id] = $profileData[$rowField->profile_field_mapping];
                                        }
                                }
                        }
                }
                $row->store();
                if (count($customFieldData))
                {
                        $rowFieldValue = JTable::getInstance('OsMembership', 'FieldValue');
                        foreach ($customFieldData as $fieldId => $fieldValue)
                        {
                                $rowFieldValue->id            = 0;
                                $rowFieldValue->field_id      = $fieldId;
                                $rowFieldValue->subscriber_id = $row->id;
                                if (is_array($fieldValue))
                                {
                                        $rowFieldValue->field_value = json_encode($fieldValue);
                                }
                                else
                                {
                                        $rowFieldValue->field_value = $fieldValue;
                                }
                                $rowFieldValue->store();
                        }
                }
        }

        return true;


        
        
        
    }

    /**
     * Display form allows users to change settings on subscription plan add/edit screen
     * @param object $row
     */
    private function drawSettingForm($row)
    {
        $params    = new JRegistry($row->params);
        $fallback_Id   = $params->get('fallbackplan_Id', '0');  //get the current fallback plan selected for this plan, set to 0 if none
        
        // Get a db connection.
        $db = JFactory::getDbo();
 
        // Create a new query object.
        $query = $db->getQuery(true);
 
        $query
        ->select($db->quoteName(array('id', 'title')))
        ->from($db->quoteName('#__osmembership_plans'))
        ->where($db->quoteName('price') . ' = '. $db->quote('0'). ' AND '. $db->quoteName('published') . " = " .  $db->quote('1') );
 
        // Reset the query using our newly populated query object.
        $db->setQuery($query);
 
        // Load the results as a list of stdClass objects (see later for more options on retrieving data).
        $freePlans = $db->loadAssocList('id', 'title');
	
        //Add a no fall back option
        $noFallBack = array ( 0 =>"No fall back plan"  );
        
        $freePlans = $noFallBack + $freePlans;

        $default = $fallback_Id;
	
	## Initialize array to store dropdown options ##
	$options = array();
	
	foreach($freePlans as $key=>$value) :
		## Create $value ##
		$options[] = JHTML::_('select.option', $key, $value);
	endforeach;

        

	$dropdown = JHTML::_('select.genericlist', $options, 'fallbackplan_Id', 'class="inputbox"', 'value', 'text', $default);

                
        ?>
        <table class="admintable adminform" style="width: 90%;">
                <tr>
                        <td width="220" class="key">
                                <?php echo JText::_('Fall Back Plan:'); ?>
                        </td>
                        <td>
                            <?php 	echo $dropdown; ?>
                        </td>
                        <td>
                                <?php echo JText::_('Select the plan that the member subscribed to this plan should change to automatically when this plan expires.'); ?>
                        </td>
                </tr>

        </table>
        <?php
    }
     
}